// Create public map 
var map;
// Static :Get user current location
var myCenter = new google.maps.LatLng(5.330666, 100.299227);
// Create public direction service and display
var directionsService = new google.maps.DirectionsService();
var directionsDisplay;

// Initialze map function
function initializeMap() {
  var mapProp = {
    center:new google.maps.LatLng(5.3814172,100.3197966),
    zoom:17,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}


// Add new marker function 

function addMarker(myPost){
    var myLocationMarker=new google.maps.Marker({
      position:myPost,
      animation:google.maps.Animation.BOUNCE
      });

    myLocationMarker.setMap(map);
    map.setCenter(myPost);
    google.maps.event.addListener(myLocationMarker,'click',function() {
      // On click , show user's current location
    });
}


function initDirectionsService() {
  directionsDisplay = new google.maps.DirectionsRenderer();
  directionsDisplay.setMap(map);
}

function calcRoute(start, end) {
  //var start = document.getElementById('start').value;
  //var end = document.getElementById('end').value;
  var request = {
      origin:start,
      destination:end,
      travelMode: google.maps.TravelMode.DRIVING//TRANSIT
  };
  
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
}

function mapJs(){
    initializeMap();
    initDirectionsService();
    addMarker(myCenter);
}
// PSC to QB

var QBLL = new google.maps.LatLng(5.331945, 100.307918);
var komtarLL = new google.maps.LatLng(5.414768, 100.330671);

var iCounter = 0;
function result1(){
    if (iCounter==5)
        return;
    var start = [];
    start[0] = new google.maps.LatLng(5.330622, 100.299271);
    start[1] = new google.maps.LatLng(5.330486, 100.299592);
    start[2] = new google.maps.LatLng(5.330417, 100.299860);
    start[3] = new google.maps.LatLng(5.330352, 100.300188);
    start[4] = new google.maps.LatLng(5.330294, 100.300386);
    var end = new google.maps.LatLng(5.331945, 100.307918);;
    calcRoute(start[iCounter],end);
    iCounter++;
}
// QB to Airport
function result2(){
    if (iCounter==5)
        return;
    var start = [];
    start[0] = new google.maps.LatLng(5.331945, 100.307918);;
    start[1] = new google.maps.LatLng(5.333504, 100.305845);
    start[2] = new google.maps.LatLng(5.331784, 100.305073);
    start[3] = new google.maps.LatLng(5.330641, 100.303732);
    start[4] = new google.maps.LatLng(5.330064, 100.301114);
    var end = new google.maps.LatLng(5.292709, 100.264799);
    calcRoute(start[iCounter],end);
    iCounter++;
}
// QB to Komtar
function result3(){
    if (iCounter==5)
        return;
    var start = [];
    start[0] = new google.maps.LatLng(5.331945, 100.307918);;
    start[1] = new google.maps.LatLng(5.344667, 100.310319);
    start[2] = new google.maps.LatLng(5.346867, 100.308881);
    start[3] = new google.maps.LatLng(5.348117, 100.309278);
    start[4] = new google.maps.LatLng(5.349645, 100.309300);
    end = new google.maps.LatLng(5.414768, 100.330671);
    calcRoute(start[iCounter],end);
    iCounter++;
}
// Komtar to Gurney
function result4(){
    if (iCounter==5)
        return;
    var start = [];
    start[0] = new google.maps.LatLng(5.414768, 100.330671);
    start[1] = new google.maps.LatLng(5.414915, 100.328961);
    start[2] = new google.maps.LatLng(5.416603, 100.329830);
    start[3] = new google.maps.LatLng(5.417388, 100.329315);
    start[4] = new google.maps.LatLng(5.418119, 100.328484);
    var end = new google.maps.LatLng(5.437740, 100.308298);
    calcRoute(start[iCounter],end);
    iCounter++;
}
google.maps.event.addDomListener(window, 'load', mapJs);
